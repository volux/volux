# Volux

[![PyPI](https://img.shields.io/pypi/v/volux?logo=python)](https://pypi.org/project/volux)
[![PyPI - Downloads](https://img.shields.io/pypi/dm/volux?logo=Python)](https://pypi.org/project/volux)
[![PyPI - License](https://img.shields.io/pypi/l/volux?color=orange&logo=Python)](https://pypi.org/project/volux)

## Installation

```bash
pip install volux
```

## Usage

For usage instructions, check out the [latest documentation](https://gitlab.com/volux/volux#volux).

## Application
❌📚 &nbsp;Looking for the `volux` **application**, *not* the Python library?

🐍 &nbsp;***[Click here](https://pypi.org/project/voluxcli) for the `voluxcli` package!***

<!-- TODO: add basic documentation -->
<!-- TODO: add basic examples -->

## Links

<!-- TODO: add website link -->
- 📖 &nbsp;[Documentation](https://gitlab.com/volux/volux#volux)
- 🐍 &nbsp;[Latest Release](https://pypi.org/project/volux)
- 🧰 &nbsp;[Source Code](https://gitlab.com/volux/volux)
- 🐞 &nbsp;[Issue Tracker](https://gitlab.com/volux/volux/-/issues)
- `🐦 Twitter` &nbsp;[@DrTexx](https://twitter.com/DrTexx)
- `📨 Email` &nbsp;[denver.opensource@tutanota.com](mailto:denver.opensource@tutanota.com)
