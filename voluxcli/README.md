# Volux CLI

[![PyPI](https://img.shields.io/pypi/v/voluxcli?logo=python)](https://pypi.org/project/voluxcli)
[![PyPI - Downloads](https://img.shields.io/pypi/dm/voluxcli?logo=Python)](https://pypi.org/project/voluxcli)
[![PyPI - License](https://img.shields.io/pypi/l/voluxcli?color=orange&logo=Python)](https://pypi.org/project/voluxcli)

## Installation

```bash
pip install voluxcli
```

## Usage

After installation, run `volux --help` for usage information.

For further instructions, check out the [latest documentation](https://gitlab.com/volux/volux#volux).

### Common Commands

| Action               | Command              | Example            |
| -------------------- | -------------------- | ------------------ |
| List available demos | `volux --list demos` | *n/a*              |
| Launch a demo        | `volux demo <name>`  | `volux demo audio` |

## Library
❌🐍 &nbsp;Looking for the **Python library**, *not* the `volux` application?

📚 &nbsp;***[Click here](https://pypi.org/project/volux) for the `volux` package!***

<!-- TODO: add basic documentation -->
<!-- TODO: add basic examples -->

## Links

<!-- TODO: add website link -->
- 📖 &nbsp;[Documentation](https://gitlab.com/volux/volux#volux)
- 🐍 &nbsp;[Latest Release](https://pypi.org/project/voluxcli)
- 🧰 &nbsp;[Source Code](https://gitlab.com/volux/volux)
- 🐞 &nbsp;[Issue Tracker](https://gitlab.com/volux/volux/-/issues)
- `🐦 Twitter` &nbsp;[@DrTexx](https://twitter.com/DrTexx)
- `📨 Email` &nbsp;[denver.opensource@tutanota.com](mailto:denver.opensource@tutanota.com)
