from voluxcontroller import VoluxController
from time import sleep


def on_presses(presses):
    # print the list of pressed buttons by standard name
    print("presses:", presses)


with VoluxController(on_presses=on_presses) as controller:
    while True:
        if controller.connected:
            print("Main thread: Controller connected!")
            while controller.connected:
                sleep(1)
        # wait until trying to enter main loop again
        sleep(0.5)
