# Volux Controller <!-- omit in toc -->

[![PyPI](https://img.shields.io/pypi/v/voluxcontroller?logo=python)](https://pypi.org/project/voluxcontroller)
[![PyPI - Downloads](https://img.shields.io/pypi/dm/voluxcontroller?logo=Python)](https://pypi.org/project/voluxcontroller)
[![PyPI - License](https://img.shields.io/pypi/l/voluxcontroller?color=orange&logo=Python)](https://pypi.org/project/voluxcontroller)

## Table of Contents <!-- omit in toc -->

- [Installation](#installation)
- [Usage](#usage)
  - [`VoluxController` - Source](#voluxcontroller---source)
    - [Examples](#examples)
- [To-do List](#to-do-list)
- [Links](#links)


## Installation 

```bash
pip install voluxcontroller
```

## Usage

### `VoluxController` - Source

#### Examples

<!-- TODO: fix this link when it breaks after moving this demo to voluxcli demos in future -->
- [Basic demo](https://gitlab.com/volux/volux/-/blob/master/voluxcontroller/demo.py) (available in the [main Volux repository](https://gitlab.com/volux/volux/))

<!-- TODO: add more example/s -->

## To-do List

<!-- TODO: add todo list (the irony) -->

- [x] Get `VoluxController` class working
- [x] Add `on_presses` callback to `VoluxController` class
- [x] Add basic example to documentation
- [ ] Replace link to basic example with a new example in a code block
- [ ] Add basic documentation
- [ ] Add more examples to documentation
- [ ] Add more detailed documentation

## Links

<!-- TODO: add website link -->
- 📖 &nbsp;[Documentation](https://gitlab.com/volux/volux#volux)
- 🐍 &nbsp;[Latest Release](https://pypi.org/project/voluxcontroller)
- 🧰 &nbsp;[Source Code](https://gitlab.com/volux/volux)
- 🐞 &nbsp;[Issue Tracker](https://gitlab.com/volux/volux/-/issues)
- `🐦 Twitter` &nbsp;[@DrTexx](https://twitter.com/DrTexx)
- `📨 Email` &nbsp;[denver.opensource@tutanota.com](mailto:denver.opensource@tutanota.com)
