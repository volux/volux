# Volux Wishlist
- [ ] Multiple moving averages for different parts of flow from an output to an input (should be configurable via GUI)
  - Example 1 (voluxvis + voluxlight):
    - Brightness moving average is 10 ticks
    - Saturation moving average is 20 ticks
  - Example 2 (voluxvis + voluxlight):
    - Treble moving average is 5 ticks
    - Bass moving average is 10 ticks
  - Essentially compute/memory efficient volux modifier module/node which only stores a single array of tick samples but can output multiple moving average ranges from the same array
    - ```
                 [AUDIO]         [MOVING-AVG MODIFIER *1]                         [VISUALIZE]          ==>   [...]
       [...] ==> BASS_AMP  ==>     INPUT_1  |  LAST_10_MOVING_AVERAGE   ==>   INPUT_1  |  BRIGHTNESS   ==>   [...]
                 (float)                    |  LAST_20_MOVING_AVERAGE   ==>   INPUT_2  |  SATURATION   ==>   [...]
      ```
      - *1 - stores n many value samples, must store at least as many as the highest average being output
- [ ] GUI for controlling volux vis workflows in real-time
- [ ] Support for MIDI as a volux source
- [ ] Support for phillips hue bulbs (if they can update fast enough + aren't absurdly expensive)
- [ ] Dynamic animation volux modifier ~~Voluxvis workflows that pulse between saturations/colors when amplitude/input low~~
  - Example 1:
    - Breathing between a pastel pink and pastel red slowly when music isn't playing or is playing low
  - Whether this is a binary thing at a given threshold or the influence of the breathing animation is derived from inputs is dependant on acceptable levels of time investment and complexity
  - Ideally keep volux modifier modular enough that animations can be plugged between arbitrary points, they then use additional inputs to influence the animation's speed/amplitude/characteristics/etc.
  - Probably best if animation modifier's primary input/outputs are both floats instead of something like outputting colours; instead if we output floats we can then decide to generate colours from them in the next step, or run them through further modifiers first
- [ ] VoluxVisualize Engine called "Spiral", just like Northern engine except instead of going from top-left pixel across and down to bottom-right, it goes from an outer corner and spiral in towards the centre of the canvas (should experiment with movement of pixels being just typical where speed is fixed and "Noise"-engine-eseqe where movement is shifted forward with amplitude as well as a base speed)
- [ ] VoluxVisualize Engine called "Hack" with a line-connected cloud of square points with underlying motion influenced by open-simplex noise (if applicable, might just need own motion logic) that's audio reactive. Envision this as strictly black background, white points/line, however worth experimenting with color (probably with a subtle/minimal palette) Audio-reactivity options: speed of points natural movement, scale of points, thickness/opacity of connecting lines, number of line connections between nodes increasing/decreasing (maybe even forming on hits up to a max and slowly decaying until the next hit). Think Watch_Dogs 1/2 kind of aesthetics, also use "Waiting for the End" Linkin Park Music video (just the square white nodes and connecting lines, not as much the other effects)
