# Volux <!-- omit in toc -->

💡 Volux is a system for crafting entertainment experiences.

🤖 It supports various local, attached and network devices.

🧩 It's modular, open-source and simple to use!

| PACKAGE                                                             | STATUS                                                                                                                                                                                                                                                                                                                                                                                             |
| ------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 📚 [volux](https://pypi.org/project/volux) (core library)            | [![PyPI](https://img.shields.io/pypi/v/volux?logo=python)](https://pypi.org/project/volux) [![PyPI - Downloads](https://img.shields.io/pypi/dm/volux?logo=Python)](https://pypi.org/project/volux) [![PyPI - License](https://img.shields.io/pypi/l/volux?color=orange&logo=Python)](https://pypi.org/project/volux)                                                                               |
| 🔊 [voluxaudio](https://pypi.org/project/voluxaudio)                 | [![PyPI](https://img.shields.io/pypi/v/voluxaudio?logo=python)](https://pypi.org/project/voluxaudio) [![PyPI - Downloads](https://img.shields.io/pypi/dm/voluxaudio?logo=Python)](https://pypi.org/project/voluxaudio) [![PyPI - License](https://img.shields.io/pypi/l/voluxaudio?color=orange&logo=Python)](https://pypi.org/project/voluxaudio)                                                 |
| **>_** [voluxcli](https://pypi.org/project/voluxcli)                | [![PyPI](https://img.shields.io/pypi/v/voluxcli?logo=python)](https://pypi.org/project/voluxcli) [![PyPI - Downloads](https://img.shields.io/pypi/dm/voluxcli?logo=Python)](https://pypi.org/project/voluxcli) [![PyPI - License](https://img.shields.io/pypi/l/voluxcli?color=orange&logo=Python)](https://pypi.org/project/voluxcli)                                                             |
| 🎮 [voluxcontroller](https://pypi.org/project/voluxcontroller)       | [![PyPI](https://img.shields.io/pypi/v/voluxcontroller?logo=python)](https://pypi.org/project/voluxcontroller) [![PyPI - Downloads](https://img.shields.io/pypi/dm/voluxcontroller?logo=Python)](https://pypi.org/project/voluxcontroller) [![PyPI - License](https://img.shields.io/pypi/l/voluxcontroller?color=orange&logo=Python)](https://pypi.org/project/voluxcontroller)                   |
| 📦 [voluxexamplemodule](https://pypi.org/project/voluxexamplemodule) | [![PyPI](https://img.shields.io/pypi/v/voluxexamplemodule?logo=python)](https://pypi.org/project/voluxexamplemodule) [![PyPI - Downloads](https://img.shields.io/pypi/dm/voluxexamplemodule?logo=Python)](https://pypi.org/project/voluxexamplemodule) [![PyPI - License](https://img.shields.io/pypi/l/voluxexamplemodule?color=orange&logo=Python)](https://pypi.org/project/voluxexamplemodule) |
| 💡 [voluxlight](https://pypi.org/project/voluxlight)                 | [![PyPI](https://img.shields.io/pypi/v/voluxlight?logo=python)](https://pypi.org/project/voluxlight) [![PyPI - Downloads](https://img.shields.io/pypi/dm/voluxlight?logo=Python)](https://pypi.org/project/voluxlight) [![PyPI - License](https://img.shields.io/pypi/l/voluxlight?color=orange&logo=Python)](https://pypi.org/project/voluxlight)                                                 |

## Table of Contents <!-- omit in toc -->
- [Installation](#installation)
- [Links](#links)
- [Demos](#demos)
- [Official Modules](#official-modules)
- [About Modules](#about-modules)
- [Development](#development)
  - [Installation](#installation-1)
  - [Building](#building)
- [Advanced Topics](#advanced-topics)
  - [Enabling Bash Completion](#enabling-bash-completion)
    - [For Developers](#for-developers)
- [To-do List](#to-do-list)

## Installation

```bash
pip install voluxcli  # for everyone
# OR
pip install volux  # for developers
```

## Links

<!-- TODO: add website link -->
- 📖 &nbsp;[Documentation](https://gitlab.com/volux/volux#volux)
- 🐍 &nbsp;Latest Releases
  - 📚 [volux](https://pypi.org/project/volux) (core library)
  - 🔊 [voluxaudio](https://pypi.org/project/voluxaudio)
  - **>_** [voluxcli](https://pypi.org/project/voluxcli)
  - 🎮 [voluxcontroller](https://pypi.org/project/voluxcontroller)
  - 📦 [voluxexamplemodule](https://pypi.org/project/voluxexamplemodule)
  - 💡 [voluxlight](https://pypi.org/project/voluxlight)
- 🧰 &nbsp;[Source Code](https://gitlab.com/volux/volux)
- 🐞 &nbsp;[Issue Tracker](https://gitlab.com/volux/volux/-/issues)
- `🐦 Twitter` &nbsp;[@DrTexx](https://twitter.com/DrTexx)
- `📨 Email` &nbsp;[denver.opensource@tutanota.com](mailto:denver.opensource@tutanota.com)

## Demos

Run `volux demo <demo-id-from-table>` to start the following demos:

| DEMO&nbsp;ID | DESCRIPTION                                                           | MODULES USED                                                |
| ------------ | --------------------------------------------------------------------- | ----------------------------------------------------------- |
| **`basic`**  | A simple `source`, `transformer` and `destination` working together.  | <ul><li>`voluxexamplemodule`</li></ul>                      |
| **`audio`**  | Simple CLI audio-visualizer.                                          | <ul><li>`voluxaudio`</li><li>`voluxexamplemodule`</li></ul> |
| **`hue`**    | Play rainbow animation on a LIFX device (e.g. smart light) in a loop. | <ul><li>`voluxlight`</li></ul>                              |

_**note:** more complex, module-specific demos can be found in the [repositories of modules](#official-modules) themselves_ 🙂

## Official Modules

| NAME                                                                                      | DESCRIPTION                                                                                                                                                                                                                                    |
| ----------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 🔊 [`voluxaudio`](https://gitlab.com/volux/voluxaudio)                                     | `source`: `VoluxAudioStream`<br /><ul><li>Simple interface to **read audio streams** on your computer.</li><li>No configuration required.</li><li>Supports computer/desktop audio 🔊</li><li>Supports external microphones 🎙️</li></ul>          |
| **>_** [`voluxcli`](https://gitlab.com/volux/volux/-/tree/master/voluxcli)                | CLI for Volux<br /><ul><li>Easy way to run demos</li></ul>                                                                                                                                                                                     |
| 🎮 [`voluxcontroller`](https://gitlab.com/volux/volux/-/tree/master/voluxcontroller)       | `source`: `VoluxController`<br /><ul><li>Get input from various **controllers/gamepads**</li></ul>                                                                                                                                             |
| 📦 [`voluxexamplemodule`](https://gitlab.com/volux/volux/-/tree/master/voluxexamplemodule) | `source`: `VoluxNumber`<br /><ul><li>Returns a randomly generated number.</li></ul>`transformer`: `VoluxMultiply`<br /><ul><li>Multiplies a number.</li></ul>`destination`: `VoluxCliBar`<br /><ul><li>Prints a bar to the terminal.</li></ul> |
| 💡 [`voluxlight`](https://gitlab.com/volux/voluxlight)                                     | `destination`: `VoluxLightLifxLan`<br /><ul><li>Control LIFX devices on the same network (e.g. **smart lights**)</li><li>Uses the [LIFX LAN protocol](https://lan.developer.lifx.com)</li></ul>                                                |

## About Modules

Volux modules are just Python modules, except the classes they contain are always subclasses of the following:

- `VoluxSource`
- `VoluxTransformer`
- `VoluxDestination`

| SUPERCLASS         | DESCRIPTION                                                                                                                                           | EXAMPLES                                                                                                                            |
| ------------------ | ----------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------- |
| `VoluxSource`      | **Outputs only**<br />Get information from host or network devices                                                                                    | <ul><li>Desktop audio amplitude</li><li>Average screen colour</li><li>Interactive GUI overlays                                      |
| `VoluxTransformer` | **Inputs and outputs**<br />Transforms given inputs into new values or types.<br /><br />_**Hint:** Chain multiple together to create data pipelines_ | <ul><li>Colour generator</li><li>Graphics processor</li><li>Value clamp</li></ul>                                                   |
| `VoluxDestination` | **Inputs only**<br />Control host or network devices in different ways                                                                                | <ul><li>Smart light control</li><li>Computer/desktop audio mixer control</li><li>Image generation</li><li>Virtual webcams</li></ul> |

## Development

### Installation

```bash
git clone https://gitlab.com/volux/volux.git
cd volux
poetry install
```

### Building

```bash
# build 'volux' dist
pushd volux && poetry install && ./development-scripts/build.sh && popd
# build 'voluxcli' dist
pushd voluxcli && poetry install && ./development-scripts/build.sh && popd
# build 'voluxexamplemodule' dist
pushd voluxexamplemodule && poetry install && ./development-scripts/build.sh && popd
```

## Advanced Topics

### Enabling Bash Completion

**NOTE:** You only need to do this once. If you're unsure if you've already enable bash completion, check your `~/.bashrc` file first.

```bash
echo '' >> ~/.bashrc
echo 'eval "$(_VOLUX_COMPLETE=bash_source volux)"' >> ~/.bashrc
```

_**note:** this will only work if `voluxcli` is already installed_

---

#### For Developers
If you're using `poetry` (recommended for development), you will need to do the following every time you want to use shell completion:

```bash
poetry shell
source enable-bash-completion.sh
```

<details>
  <summary><b>Explanation...</b></summary>
  
This appears to be due to `poetry shell` loading `~/.bashrc` before it adds the corresponding virtualenv's `[...]/bin` directory to the `$PATH`.
You can test this by temporarily adding `echo $PATH` to the end of your `~/.bashrc` file and then opening a virtualenv with `poetry shell`.

Importantly shell completion only works if it can _execute code from the `voluxcli` dist_ (this is how [click](https://pypi.org/project/click/) provides shell completion). Therefore if `voluxcli` is inaccessible in your virtual environment when `~/.bashrc` is loaded, an error will occur and shell completion will not work.

</details>

## To-do List

- [x] Add PyPI links to README's of all published modules, either in own repos or inside Volux monorepo
  - [x] volux
  - [x] voluxcli
  - [x] voluxexamplemodule
  - [x] voluxaudio
  - [x] voluxlight
- [ ] Consider adding flair to Volux monorepo README headings with emojis
- [ ] Add visual explanations/graphs to Volux monorepo README, ideally colourful but not essential
- [ ] Add video/gif demos for each of the demos described in the Volux monorepo READMEs demos table (maybe inside the table?)
- [ ] Consider including other repos that aren't part of the monorepo inside it as subdirs using that weird git link thingy where it shows up inside the repositories directory structure somehow (something about git modules?)
