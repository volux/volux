# Volux Example Module

[![PyPI](https://img.shields.io/pypi/v/voluxexamplemodule?logo=python)](https://pypi.org/project/voluxexamplemodule)
[![PyPI - Downloads](https://img.shields.io/pypi/dm/voluxexamplemodule?logo=Python)](https://pypi.org/project/voluxexamplemodule)
[![PyPI - License](https://img.shields.io/pypi/l/voluxexamplemodule?color=orange&logo=Python)](https://pypi.org/project/voluxexamplemodule)

## Installation

```bash
pip install voluxexamplemodule
```

## Usage

For usage instructions, check out the [latest documentation](https://gitlab.com/volux/volux#volux).

## Application
❌📚 &nbsp;Looking for the `volux` **application**?

🐍 &nbsp;***[Click here](https://pypi.org/project/voluxcli) for the `voluxcli` package!***

<!-- TODO: add basic documentation -->
<!-- TODO: add basic examples -->

## Links

<!-- TODO: add website link -->
- 📖 &nbsp;[Documentation](https://gitlab.com/volux/volux#volux)
- 🐍 &nbsp;[Latest Release](https://pypi.org/project/voluxexamplemodule)
- 🧰 &nbsp;[Source Code](https://gitlab.com/volux/volux)
- 🐞 &nbsp;[Issue Tracker](https://gitlab.com/volux/volux/-/issues)
- `🐦 Twitter` &nbsp;[@DrTexx](https://twitter.com/DrTexx)
- `📨 Email` &nbsp;[denver.opensource@tutanota.com](mailto:denver.opensource@tutanota.com)
