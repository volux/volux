# --- PSEUDO CODE BELOW##
# define a class which always checks if lifxlan device objects supports frames of any sets of n many dimensions
# e.g. does a strip support 3 dimensions visualizers? what about 2 dimension vis? what about 1 dimensions vis?
class VlxLifxVisDevice:
    def __init__(self, raw_device, state_settings={states_stored: 2}):
        # this would be defined as a constant at the top of the file defining this class
        support_3D = ["room"]
        support_2D = ["room", "tile"]
        support_1D = ["room", "tile", "strip"]
        # include in all other places that do things related to 1D/2D/3D frames
        support_0D = ["room", "tile", "strip", "bulb"]

        # set attributes based on support for frames of n dimensions
        self.supports_3D_engines = raw_device.device_type in support_3D
        self.supports_2D_engines = raw_device.device_type in support_2D
        self.supports_1D_engines = raw_device.device_type in support_1D
        # TODO: ensure states_stored is a positive integer before checking
        self.is_stateful = state_settings.states_stored > 0

        # if vis device is stateful
        if self.is_stateful is True:
            # if vis device supports 3D engines
            if self.supports_3D_engines:
                # set a frames_3d attribute, also init it with an empty frame
                self.frames_3d = [
                    generate_empty_3d_frame()
                    for _ in range(state_settings.states_stored)
                ]
            # ... same as 3D but for 2D
            if self.supports_2D_engines:
                self.frames_2d = [
                    generate_empty_2d_frame()
                    for _ in range(state_settings.states_stored)
                ]
            # ... same as 3D but for 1D
            if self.supports_1D_engines:
                self.frames_1d = [
                    generate_empty_1d_frame()
                    for _ in range(state_settings.states_stored)
                ]


vis_devices = []
# "raw_devices" are all lifxlan device objects
for raw_device in raw_devices:
    vis_devices.append(VlxLifxVisDevice(raw_device))

vis_3d_devices = []
vis_2d_devices = []
vis_1d_devices = []
# add devices to arrays according to number of dimensions supported
for vis_device in vis_devices:
    if vis_device.supports_3D_engines is True:
        vis_3d_devices.append(vis_device)
    if vis_device.supports_2D_engines is True:
        vis_2d_devices.append(vis_device)
    if vis_device.supports_1D_engines is True:
        vis_1d_devices.append(vis_device)

# check which dimension-focused device arrays actually have contents
any_3d_devices = len(vis_3d_devices) > 0
any_2d_devices = len(vis_2d_devices) > 0
any_1d_devices = len(vis_1d_devices) > 0

# initialize an object that has a method to get audio levels
audio_getter = AudioLevelGetterThing()
# initialize vis engine
engine = VisEngine(preferences={})

frame_3d = generate_empty_3d_frame()
frame_2d = generate_empty_2d_frame()
frame_1d = generate_empty_1d_frame()
# start main loop
while True:
    # get levels from a method that can get audio data, should return an array of values, # of vals determined by bands split
    levels = audio_getter.get_audio()

    # only generate frames of n dimensions if there are devices that support frames of n dimensions
    if any_3d_devices is True:
        frame_3d = engine.gen_frame_3d(levels)
    if any_2d_devices is True:
        frame_2d = engine.gen_frame_2d(levels)
    if any_1d_devices is True:
        frame_1d = engine.gen_frame_1d(levels)

    # for every device supporting frames of n dimensions, set real devices to a frame of n dimensions
    for device_3d in vis_3d_devices:
        vis_device.set_color_3D(frame_3d)
    for device_2d in vis_2d_devices:
        vis_device.set_color_2D(frame_2d)
    for device_1d in vis_1d_devices:
        vis_device.set_color_1D(frame_1d)
# --- PSEUDO CODE ABOVE
