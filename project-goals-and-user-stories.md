# Volux - User Stories

## Visual Programming Interface

- [ ] As a user, I want to be able to create automations easily.
- [ ] As a user, I want to be able to create some simple automations without installing any extra modules.
- [ ] As a user, I want to be able to browse and search for different modules I can install.
- [ ] As a user, I want to be able to install modules in the app with one click.
- [ ] As a user, I want to be able to quickly jump back into editing my recently opened automations
- [ ] As a user, I want to be able to share my automations as files

- [ ] As a developer, I want to be able to easily create my own modules
- [ ] As a developer, I want to be able to easily publish modules I create
